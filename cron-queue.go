package main

import (
	"github.com/gorhill/cronexpr"
	"container/heap"
	"github.com/dilip/cron/types"
	"time"
	"fmt"
	//"reflect"
	"sync"
	"errors"
)



type Item struct {
	Cron types.Cron
	//priority time.Duration
	index int
}

func (e *Item) Priority() time.Duration {
	return e.Cron.NextRun.Sub(time.Now())
}

type PriorityQueue []*Item

type CronQueue struct {
	pq PriorityQueue
	mutex sync.RWMutex
}


func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	// We want Pop to give us the lowest, not highest, priority so we use less than here.
	return pq[i].Priority() < pq[j].Priority()
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *PriorityQueue) Push(x interface{}) {
	n := len(*pq)
	item := x.(*Item)
	item.index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	item.index = -1 // for safety
	*pq = old[0 : n-1]
	return item
}

// update modifies the priority and value of an Item in the queue.
// func (pq *PriorityQueue) update(item *Item, value string, priority int) {
// 	item.value = value
// 	item.priority = priority
// 	heap.Fix(pq, item.index)
// }


func NewCronQueue() *CronQueue {
	pq := make(PriorityQueue, 0)
	heap.Init(&pq)
	return &CronQueue{pq: pq}
}


func (cq *CronQueue) Pop() (types.Cron, error) {
	if len(cq.pq) <= 0 {
		return types.DefaultCron(), errors.New("Queue is empty")
	}
	item := heap.Pop(&cq.pq).(*Item)
	return item.Cron, nil
}

func (cq *CronQueue) Push(c types.Cron) {
	//priority := c.NextRun.Sub(time.Now())
	item := &Item{
		Cron: c,
		//priority: priority,
	}
	heap.Push(&cq.pq, item)
}

func (cq *CronQueue) Update(c types.Cron) {
	for _, d := range cq.pq {
		if d.Cron.Id == c.Id {
			//priority := c.NextRun.Sub(time.Now())
			//d.priority = priority
			d.Cron = c
			heap.Fix(&cq.pq, d.index)
			break
		}
	}
}

func ManageCrons() {
	cq := NewCronQueue()
	nc, _ := cq.Pop()
	
	for {
		select {
		case a := <-newCrons:
			if a.NextRun.Before(nc.NextRun) {
				fmt.Println("Previous next Scheduled cron ::", nc)
				fmt.Println("New next Scheduled cron ::", a)
				cq.Push(nc)
				nc = a
				nextCron <- nc
			} else {
				cq.Push(a)
			}
		case b := <-updatedCrons:
			if b.NextRun.Before(nc.NextRun) {
				cq.Update(b)
				cq.Push(nc)
				nc, _ = cq.Pop()
				nextCron <- nc
			} else {
				cq.Update(b)
			}
		case <-getNextCron:
			nc.NextRun = cronexpr.MustParse(nc.Schedule).Next(nc.NextRun)
			cq.Push(nc)
			nc, _ = cq.Pop()
			nextCron <- nc
		}
	}
}