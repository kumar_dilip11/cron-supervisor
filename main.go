package main

import (
	"fmt"
	"github.com/gorhill/cronexpr"
	"time"
	//"reflect"
	"encoding/json"
	"github.com/dilip/cron/types"
	"log"
	"net/http"
	"net/url"
	"sync"
)

type crons struct {
	lock sync.RWMutex
	cron map[int]types.Cron
}

type latestCrons struct {
	Id       int    `json:"id"`
	Schedule string `json:"schedule"`
	Command  string `json:"command"`
	Overlap  int    `json:"overlap"`
}

const cronUpdateInterval = 20 * time.Second
const clusterId = "1"
const urlGetCrons = "http://127.0.0.1:8886/clusters/%s/crons"

var runningCrons = make(map[int]int)
var wg sync.WaitGroup

var nextCron = make(chan types.Cron, 1)
var newCrons = make(chan types.Cron)
var updatedCrons = make(chan types.Cron)
var expiredCron = make(chan types.Cron)
var getNextCron = make(chan bool, 1)
var timer = time.NewTimer(0 * time.Second)
var currentCron types.Cron

func main() {
	go ManageCrons()
	go manageUpcomingCron()
	go cronTimer()

	x := crons{cron: make(map[int]types.Cron)}
	x.update()
}

func manageUpcomingCron() {
	for {
		select {
		case c := <-nextCron:

			if !timer.Stop() {
				select {
				case <-timer.C:
				default:
					fmt.Println("breaking")
				}
			}
			currentCron = c
			duration := c.NextRun.Sub(time.Now())
			//fmt.Println(time.Now(), " Starting cron ", c, " in next ", duration, "seconds")
			fmt.Println("Next Cron at ", c.NextRun, " Id::", c.Id, " Schedule::", c.Schedule)
			if duration < 0 {
				expiredCron <- c
			} else {
				timer.Reset(duration)
			}
		}
	}
}

func cronTimer() {
	for {
		select {
		case <-timer.C:
			go runCron(currentCron)
			getNextCron <- true
		case a := <-expiredCron:
			go runCron(a)
			getNextCron <- true
		}
	}
}

func runCron(c types.Cron) {
	fmt.Println(time.Now(), "running cron", c)
	//fmt.Println(c)
}

func isCronUpdated(lc latestCrons, oc *types.Cron) bool {
	updated := false
	if lc.Schedule != oc.Schedule {
		oc.Schedule = lc.Schedule
		oc.NextRun = cronexpr.MustParse(lc.Schedule).Next(time.Now())
		updated = true
	}
	if lc.Command != oc.Command {
		oc.Command = lc.Command
		updated = true
	}

	return updated
}

func jsonToCron(lc latestCrons) types.Cron {
	nr := cronexpr.MustParse(lc.Schedule).Next(time.Now()) //next run of the cron
	nc := types.Cron{Id: lc.Id, Schedule: lc.Schedule, Command: lc.Command, Overlap: lc.Overlap, NextRun: nr}
	return nc
}

func (c *crons) update() {
	for {
		fmt.Println(time.Now(), "starting cron update...")
		l := fetchCrons()
		for _, lc := range l {
			var oc types.Cron //old cron
			var ok bool

			oc, ok = c.cron[lc.Id]

			if ok {
				if isCronUpdated(lc, &oc) {
					updatedCrons <- oc
				}
			} else {
				nc := jsonToCron(lc)
				c.cron[lc.Id] = nc
				newCrons <- nc
			}
		}
		fmt.Println(time.Now(), "cron update ended...")
		time.Sleep(cronUpdateInterval)
	}
}

func fetchCrons() []latestCrons {
	lc := []latestCrons{}

	safeClusterId := url.QueryEscape(clusterId)
	url := fmt.Sprintf(urlGetCrons, safeClusterId)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal("NewRequest: ", err)
		return lc
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Do: ", err)
		return lc
	}

	defer resp.Body.Close()

	if err := json.NewDecoder(resp.Body).Decode(&lc); err != nil {
		log.Println(err)
		return lc
	}

	return lc
}
