package types


import (
	"time"
)





type Cron struct {
	Id int
	Schedule string
	Command string
	// if the process is already running then it should wait to end it or
	// just start. Helps if process takes more than normal time and another schedule starts.
	Overlap int
	// time it will run next
	NextRun time.Time
	// status of the process. running or stopped. if running and Overlap
	// is true then it will run again else wait untill the program ends.
	status string
	// if laps > 0 means cron is overlaped (means it's next schedule is passed but it's still running)
	// it tells how many times it's overlapped
	laps int
	// if waiting is true means cron is waiting to finish previous run.
	// so as soon as cron is finished it will start running again
	waiting bool
	// remove asap
	// only to support old code
	ForceRun bool
}

func DefaultCron() Cron {
	return Cron{Id:0, Schedule:"*/5 * * * *", NextRun:time.Now()}
}
